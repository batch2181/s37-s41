const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const user = require("../models/user");

module.exports.checkEmailExist = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0){
            return true;
        } else {
            return false;
        }
    })
};

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    })

    console.log(newUser);
    
    return newUser.save().then((user, error) => {
        if(error){
            return false;
        } else {
            return true;
        }
    })

};

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return false
        } else {
            // compareSync is to compare an unhashed password to hashed password
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect){
                return { access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
}

module.exports.checkDetails = (reqBody) => {
    return User.findById({_id: reqBody._id}).then(result => {
        if(result == null){
            return false
        } else {
            result.password = "******"
            return result
        }
    })
}

// Enroll user to a class
module.exports.enroll = async (data) => {

    let isUserUpdated = await User.findById(data.userId).then(user => {
        user.enrollments.push({courseId: data.courseId});
        
        return user.save().then((user, error) => {
            if(error){
                return false
            } else {
                return true
            }
        })
    })

    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        course.enrollees.push({userId: data.userId});

        return course.save().then((course, error) => {
            if(error){
                return false
            } else {
                return true
            }
        })
    })

    if(isUserUpdated && isCourseUpdated){
        return true;
    } else {
        return false;
    }
}
