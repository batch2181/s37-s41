const Course = require("../models/course");
const User = require('../models/user');

// S39 ACTIVITY
// Create a Single Course
module.exports.addCourse = (data) => {
    console.log(data.isAdmin)

    if(data.isAdmin) {
        let newCourse = new Course({
            name: data.course.name,
            description: data.course.description,
            price: data.course.price
        });

        return newCourse.save().then((newCourse, error) => {
            if(error){
                return error
            }

            return newCourse 
        })
    };

    // If the user is not admin, then return this message as a promise to avoid errors
    let message = Promise.resolve('User must be ADMIN to access this.')

    return message.then((value) => {
        return {value}
    })
};



module.exports.getAllCourse = () => {
    return Course.find({}).then(result => {
        return result
    })
}


module.exports.getActiveCourse = () => {
    return Course.find({isActive: true  }).then(result => {
        return result
    })
}

module.exports.getSpecificCourse = (courseId) => {
    return Course.findById(courseId).then(result => {
        if(result == null){
            return false
        } else {
            console.log(result)
            return result
        }
    })
}

module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Course.findByIdAndUpdate(courseId , 
			{			//req.body  
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updatedCourse, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
};


// S40 ACTIVITY
module.exports.archiveCourse = (courseId) => {
	return Course.findByIdAndUpdate(courseId, {
		isActive: false
	})
	.then((archivedCourse, error) => {
		if(error){
			return false
		} 

		return true
	})
};

