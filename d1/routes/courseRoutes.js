const express = require("express")
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

router.post("/create", (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
    courseController.addCourse(req.body).then(resultFromController => 
        res.send(resultFromController));
})

router.get("/all", (req, res) => {
    courseController.getAllCourse(req.body).then(resultFromController => 
        res.send(resultFromController));
})

router.get("/active", (req, res) => {
    courseController.getActiveCourse(req.body).then(resultFromController => 
        res.send(resultFromController));
})

router.get("/:courseId", (req, res) => {
    courseController.getSpecificCourse(req.params.courseId).then(resultFromController => 
        res.send(resultFromController))
})

router.patch("/:courseId/update", auth.verify, (req,res) => 
{
	const newData = {
		course: req.body, 	//req.headers.authorization contains jwt
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})


// S40 ACTIVITY
// Archiving a single course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => {
		res.send(resultFromController)
	})
})









module.exports = router;